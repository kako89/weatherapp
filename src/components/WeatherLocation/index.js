import React, {Component} from 'react';
import Location from './Location';
import WeatherData from './WeatherData';
import {SUN} from './../../constants/weathers';
import './styles.css';

const location = "santiago,cl";
const api_key ="f99bbd9e4959b513e9bd0d7f7356b38d";
const api_weather=`http://api.openweathermap.org/data/2.5/weather?q=${location}&appid=${api_key}`;


const data1={
    temperature: 20,
    weatherState:SUN,
    humidity:5,
    wind:'19 m/s',
};

/*const data2={
    temperature: 18,
    weatherState:WINDY,
    humidity:10,
    wind:'10 m/s',
};*/


class WeatherLocation extends Component {

    constructor(){
        super();
        this.state={
            city:"Buenos Aires",
            data:data1
        };
    }

    handleUpdateClick = ()=>{
        fetch(api_weather).then(data=>{
            console.log(data);
            return data.json();
        }).then(weather_data =>{
            console.log(weather_data);
        });
        
        /*this.setState({
            data: data2
        });*/
        console.log("Actualizado");
    }

    render = () => {
        const {city, data}=this.state;
        return(
        <div className='weatherLocationCont'>
            <Location city={city}/>
            <WeatherData data={data}/>
            <button  onClick={this.handleUpdateClick}>Actualizar</button>
        </div>);
    };

}



export default WeatherLocation;